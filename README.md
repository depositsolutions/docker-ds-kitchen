# Custom Test-Kitchen Build Container
[![pipeline status](https://gitlab.com/depositsolutions/docker-ds-kitchen/badges/master/pipeline.svg)](https://gitlab.com/depositsolutions/docker-ds-kitchen/commits/master)

This project contains the dockerfile for a test-kitchen base image of gitlab-ci jobs.

## Dependencies
  * rake
  * test-kitchen
  * kitchen-docker
  * kitchen-inspec
  * inspec
  * kitchen-salt

## HowTo
Build docker image to update the included packages
```
docker build . -t  registry.gitlab.com/depositsolutions/docker-ds-kitchen:latest
```

Upload image to the registry
```
docker push registry.gitlab.com/depositsolutions/docker-ds-kitchen:latest
```

Use image in gitlab-ci.yml
```
image: registry.gitlab.com/depositsolutions/docker-ds-kitchen:latest
```
