FROM docker:latest

RUN echo gem: --no-document > $HOME/.gemrc
RUN apk update && apk add build-base git libffi-dev ruby-dev ruby-bundler
COPY Gemfile* /tmp/
WORKDIR /tmp
RUN bundler install
